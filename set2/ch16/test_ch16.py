import unittest
import binascii
from ch16 import (sanitize,
        count_consecutive_zeros)

class test_16(unittest.TestCase):
    def setUp(self):
        pass
    def test_sanitize(self):
        raw = b';asd=dsa;'
        reference = b'asddsa'
        out = sanitize(raw)
        self.assertEqual(out, reference)
        
        raw = b';;;asd===dsa;;;'
        reference = b'asddsa'
        out = sanitize(raw)
        self.assertEqual(out, reference)

        raw = b''
        reference = b''
        out = sanitize(raw)
        self.assertEqual(out, reference)

    def test_count_zero(self):
        buf = binascii.unhexlify('00000001010203010203')
        test = count_consecutive_zeros(buf)
        ref = 3
        self.assertEqual(test, ref, msg='Failed only starting zeros')

        buf = binascii.unhexlify('01010203010203')
        test = count_consecutive_zeros(buf)
        ref = 0
        self.assertEqual(test, ref, msg='Failed no zeros')
        
        buf = binascii.unhexlify('01010203010203000000')
        test = count_consecutive_zeros(buf)
        ref = 0
        self.assertEqual(test, ref, msg='Failed only ending zeros')
        
        buf = binascii.unhexlify('00000001010203010203000000')
        test = count_consecutive_zeros(buf)
        ref = 3
        self.assertEqual(test, ref, msg='Failes zeros at both ends')

if __name__ == '__main__':
    unittest.main()
