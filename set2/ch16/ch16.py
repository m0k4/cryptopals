import sys
sys.path.append('../../libs')
import cryptopals

def sanitize(raw_input: bytes) -> bytes:
    """sanitize: sanitise the input by removing ';' and '=' characters.
    Returns the sanitised input.

    :param raw_input: the raw input
    :type raw_input: bytes

    :rtype: bytes
    """
    raw_input = raw_input.replace(b';', b'')
    raw_input = raw_input.replace(b'=', b'')
    return raw_input

def oracle_encryption_cbc(plaintext: bytes) -> bytes:
    key = b'THIS IS RANDOM:)'
    iv = b'YELLOW SUBMARINE'
    pre = b'comment1=cooking%20MCs;userdata='
    post = b';comment2=%20like%20a%20pound%20of%20bacon'
    plaintext = sanitize(plaintext)
    plaintext = pre+plaintext+post
    ciphertext = cryptopals.encrypt_AES_128_CBC(plaintext, key, iv)
    return ciphertext

def oracle_decryption_cbc_match(ciphertext: bytes) -> bool:
    key = b'THIS IS RANDOM:)'
    iv = b'YELLOW SUBMARINE'
    plaintext = cryptopals.decrypt_AES_128_CBC(ciphertext, key, iv)
    print_blocks(cryptopals.pad_pkcs7(plaintext, 16))
    match = plaintext.find(b';admin=true;')
    if match != -1:
        return True
    else:
        return False

def print_blocks(buf: bytes, bsize: int=16):
    """print_blocks: print the buffer "buf" with  separator character "|" every
    "bsize" bytes.

    :param buf: the buffer
    :type buf: bytes
    :param bsize: the block size
    :type bsize: int
    """
    if len(buf)%bsize != 0:
        raise ValueError
    for i in range(len(buf)//bsize):
        print(buf[bsize*i:bsize*i+bsize], end='|')
    print()

def count_consecutive_zeros(buf: bytes) -> int:
    """count_consecutive_zeros: count the consecutive zeros starting from
    position 0 in the buffer.
    Return the number of consecutive zeros.

    :param buf: the buffer
    :type buf: bytes

    :rtype: int
    """
    count = 0
    for i in buf:
        if i != 0:
            break
        else:
            count += 1
    return count

if __name__ == '__main__':
    bsize = 16
    # start with an empty input and see the difference with a one-byte input,
    pad = b''
    ciphertext_prev = oracle_encryption_cbc(pad)
    pad = b'a'
    ciphertext_step = oracle_encryption_cbc(pad)
    #  get the number of complete blocks are used by the random prefix
    xored = cryptopals.xor_buf(ciphertext_prev, ciphertext_step)
    start_equal_bytes = count_consecutive_zeros(xored)
    start_eq_blocks = start_equal_bytes//bsize
    ciphertext_prev = ciphertext_step
    # increment the input to see if the random prefix is partially using
    # another block
    while True:
        pad += b'a'
        ciphertext_step = oracle_encryption_cbc(pad)
        xored = cryptopals.xor_buf(ciphertext_prev, ciphertext_step)
        # get how many blocks and bytes are not changed from the previous input
        equal_bytes = count_consecutive_zeros(xored)
        eq_blocks, eq_bytes = (equal_bytes//bsize, equal_bytes%bsize)
        # if we filled another block we have to stop
        if eq_blocks - start_eq_blocks >= 1:
            pad = pad[:-1]
            print('added {0} bytes'.format(len(pad)))
            break
        ciphertext_prev = ciphertext_step
    # this is out target block, the 'X'es are the character where the forbidden
    # characters would go
    dummy = b'X'
    target = b'aaaaa'+dummy+b'admin'+dummy+b'true'
    # the offsets of the forbidden characters
    o1 = 5
    o2 = 11
    # if the padding is 16 bytes it means that the random prefix was a multiple
    # of the blocksize and we already have one "empty block"
    if len(pad)==16:
        # the position of the bytes to change
        p1 = start_eq_blocks*bsize+o1
        p2 = start_eq_blocks*bsize+o2
        plaintext = pad + target
    # otherwise we just filled a partially used block and we need to add the
    # "empty block"
    else:
        # the position of the bytes to change
        p1 = eq_blocks*bsize+o1
        p2 = eq_blocks*bsize+o2
        plaintext = pad + b'a'*bsize + target
    # get the ciphertext using the crafted input
    ciphertext = bytearray(oracle_encryption_cbc(plaintext))
    # change the value of the ciphertext in order to obtain the desired values
    # once decrypted
    ciphertext[p1] = ciphertext[p1]^ord('X')^ord(';')
    ciphertext[p2] = ciphertext[p2]^ord('X')^ord('=')
    # check if we got the desired string in the plaintext
    plain_tmp = oracle_decryption_cbc_match(bytes(ciphertext))
    if plain_tmp == True:
        print("Admin flag true")

