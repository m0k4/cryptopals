import base64
import sys
sys.path.append('../../libs')
from cryptopals import (encrypt_AES_128_ECB,
        decrypt_AES_128_ECB,
        find_repeating_blocks,
        generate_random_bytes,
        pad_pkcs7,
        unpad_pkcs7)
from random import SystemRandom

def encryption_oracle_ecb(plaintext: bytes) -> bytes:
    """encryption_oracle_ecb: encryption oracle using AES 128 with ECB and a
    secret, constant key. Before encrypting, a secret string gets appended
    after the plaintex.

    Returns the ciphertext.

    :param plaintext: the plaintext to encrypt, NOT padded
    :type plaintext: bytes
    
    :return: the ciphertext
    :rtype: bytes
    """

    secret = ('Um9sbGluJyBpbiBteSA1LjAKV2l0aCBteSByYWctdG9wIGRvd24gc28gbXkg'
    'aGFpciBjYW4gYmxvdwpUaGUgZ2lybGllcyBvbiBzdGFuZGJ5IHdhdmluZyBq'
    'dXN0IHRvIHNheSBoaQpEaWQgeW91IHN0b3A/IE5vLCBJIGp1c3QgZHJvdmUg'
    'YnkK')
    key = b'YELLOW SUBMARINE'
    plaintext += base64.b64decode(secret)
    plaintext = pad_pkcs7(plaintext, 16)
    ciphertext = encrypt_AES_128_ECB(plaintext, key)

    return ciphertext

def find_blocksize(oracle: encryption_oracle_ecb) -> (int, int):
    """find_blocksize: find the block size up to a maximum of 512 bits.
    Returns a tuple of int containing the block size and the number of filling
    characters entered to get a new block. If the blocksize was not found,
    returns 0.

    :param oracle:
    :type oracle: encryption_oracle_ecb

    :rtype: tuple
    """
    # max block size to check for 
    bsize_max = 64
    plaintext = b''
    ciphertext = oracle(plaintext)
    ct_len = len(ciphertext)
    blocksize = 0
    for i in range(bsize_max+1):
        plaintext += b'A'
        ciphertext = oracle(plaintext)
        if len(ciphertext) != ct_len:
            blocksize = len(ciphertext)-ct_len
            break

    return blocksize, i+1

def test_ecb(oracle: encryption_oracle_ecb, bsize: int) -> bool:
    """test_ecb: test for ECB cipher mode by encrypting with the oracle
    function two block with the same value and checking if the resulting
    ciphertext have two equal blocks.

    Return True if the mode is ECB, False otherwise.

    :param oracle: the oracle function
    :type oracle: encryption_oracle_ecb
    :param bsize: the block size in byte to use
    :type bsize: int

    :return: True if ECB, False otherwise
    :rtype: bool
    """
    pt_same_2_block = b'A'*bsize*2
    ct_same_2_block = encryption_oracle_ecb(pt_same_2_block)
    blocks = find_repeating_blocks(ct_same_2_block, bsize)
    ecb = False
    for i in blocks.values():
        if i > 1:
            ecb = True
    return ecb

def byte_at_a_time_post(oracle: encryption_oracle_ecb, bsize: int) -> bytes:
    """byte_at_a_time_post

    :param oracle:
    :type oracle: encryption_oracle_ecb
    :param bsize:
    :type bsize: int

    :rtype: bytes
    """
    print('[+] Trying byte at a time attack:')
    # store here the recovered bytes
    recovered = bytearray()
    # start with a plaintext one byte short of the blocksize
    plaintext = b'A'*(bsize-1)
    # compute how many blocks we have to crack
    ciphertext = oracle(b'')
    if len(ciphertext)%bsize != 0:
        raise ValueError
    n_blocks = int(len(ciphertext)/bsize)
    # get the length of the secret text
    bsize, filling = find_blocksize(encryption_oracle_ecb)
    s_length = len(ciphertext)-filling
   
    # compute just once all the plaintexts (P_n) and corresponding
    # ciphertexts(C_n) then store them in ct_store
    ct_store = {}
    for i in range(bsize):
        plaintext = b'A'*(bsize-1-i)
        ciphertext = oracle(plaintext)
        ct_store[i] = {}
        ct_store[i]['C'] = ciphertext
        ct_store[i]['P'] = plaintext
    
    # decrypt one block at a time
    for b in range(n_blocks):
        # cycle through every byte in the block
        for i in range(bsize):
            # try every possible value [0, 255] in one byte
            for k in range(256):
                # craft the input using the current plaintext P_n, the bytes
                # recovered so far and the current test value
                crafted_input = ct_store[i]['P']+recovered+k.to_bytes(1, sys.byteorder)
                # get the ciphertext using the crafted input
                cipher_crafted = oracle(crafted_input)
                # check if the b-th blocks of the two ciphers match
                if (cipher_crafted[b*bsize:b*bsize+bsize] ==
                        ct_store[i]['C'][b*bsize:b*bsize+bsize]):
                    # if they match, we got a new byte. store it
                    recovered += k.to_bytes(1, sys.byteorder)
                    sys.stdout.write('\r recovered {0}/{1} bytes: {2}'.format(
                        len(recovered),
                        s_length,
                        repr(recovered.decode())
                        ))
                    # check if we recovered all the characters
                    if len(recovered) == s_length:
                        sys.stdout.write('\n')
                        sys.stdout.flush()
                        #return the secret string
                        return recovered
                    # exit the bruteforce loop
                    break

if __name__ == '__main__':
    blocksize, filling = find_blocksize(encryption_oracle_ecb)
    print('[+] Block size: %s' %blocksize)
    ecb = test_ecb(encryption_oracle_ecb, blocksize)
    print('[+] ECB mode: %s'% ecb)
    plaintext = byte_at_a_time_post(encryption_oracle_ecb, blocksize)
    print(plaintext)
