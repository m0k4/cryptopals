from random import randrange, SystemRandom
import sys
sys.path.append('../../libs')
from cryptopals import (encrypt_AES_128_ECB,
        encrypt_AES_128_CBC,
        find_repeating_blocks,
        pad_pkcs7,
        unpad_pkcs7)

def generate_random_bytes(bsize: int) -> bytes:
    """generate_random_bytes: return a bytes-like object of size 'bsize'
    containing random content.

    :param bsize: the size of the buffer
    :type bsize: int
    
    :return: a random buffer of size 'bsize'
    :rtype: bytes
    """
    rand = bytearray(bsize)
    rng = SystemRandom()
    for i in range(len(rand)):
        rand[i] = rng.randrange(256)
    return bytes(rand)

def encryption_oracle(plaintext: bytes) -> (bytes, str):
    """encryption_oracle: an encryption oracle that:
    + append before the given plaintext a random value buffer of random size
    between 5 and 10 bytes.
    + append after the given plaintext a random value buffer of random size
    between 5 and 10 bytes.
    + use randomly (0.5 probability each) AES 128 CBC or AES 128 ECB
    + generate a random key
    + generate a random IV
    
    Returns a tuple containing the ciphertext and a string that state the block
    mode used:
    + 0 for ECB
    + 1 for CBC

    :param bytes: the plaintext
    :param str:

    :rtype: (bytes,str)
    """
    rng = SystemRandom()
    pre_size = rng.randrange(5,11)
    pre = generate_random_bytes(pre_size)
    post_size = rng.randrange(5,11)
    post = generate_random_bytes(post_size)
    plaintext = pre + plaintext + post
    key = generate_random_bytes(16)
    mode = rng.randrange(0,2)
    if mode == 0:
        # use ECB
        plaintext = pad_pkcs7(plaintext, 16)
        ciphertext = encrypt_AES_128_ECB(plaintext, key)
    elif mode == 1:
        #use CBC
        iv = generate_random_bytes(16)
        ciphertext = encrypt_AES_128_CBC(plaintext, key, iv)
    else:
        raise Exception
    return ciphertext, mode

def detect_enc_mode(oracle: encryption_oracle, min_plain_size: int) -> int:
    bsize = 16
    plaintext = bytes(randrange(256))*min_plain_size
    ciphertext, mode = encryption_oracle(plaintext)
    blocks = find_repeating_blocks(ciphertext, bsize)
    repeating = blocks.values()
    for i in iter(repeating):
        if i > 1:
            return 0, mode
    return 1, mode

if __name__ == '__main__':
    for i in range(10):
        min_plain_size = 43
        guessed_mode, mode = detect_enc_mode(encryption_oracle, min_plain_size)
        if guessed_mode == mode:
            print('✓')
        else:
            print('✕')
        print('-'*10)
