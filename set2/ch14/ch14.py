import base64
import sys
sys.path.append('../../libs')
from cryptopals import (encrypt_AES_128_ECB,
        decrypt_AES_128_ECB,
        find_repeating_blocks,
        pad_pkcs7,
        unpad_pkcs7)
import random

def encryption_oracle_ecb_pre_post(plaintext: bytes) -> bytes:
    """encryption_oracle_ecb: encryption oracle using AES 128 with ECB and a
    secret, constant key. Before encrypting, a secret string gets appended
    before and after the plaintex.

    Returns the ciphertext.

    :param plaintext: the plaintext to encrypt, NOT padded
    :type plaintext: bytes
    
    :return: the ciphertext
    :rtype: bytes
    """
    
    pre = b'This is just random text: ksjdahaaaskljfd'
    secret = ('Um9sbGluJyBpbiBteSA1LjAKV2l0aCBteSByYWctdG9wIGRvd24gc28gbXkg'
    'aGFpciBjYW4gYmxvdwpUaGUgZ2lybGllcyBvbiBzdGFuZGJ5IHdhdmluZyBq'
    'dXN0IHRvIHNheSBoaQpEaWQgeW91IHN0b3A/IE5vLCBJIGp1c3QgZHJvdmUg'
    'YnkK')
    key = b'YELLOW SUBMARINE'
    plaintext += base64.b64decode(secret)
    plaintext = pre + plaintext
    plaintext = pad_pkcs7(plaintext, 16)
    ciphertext = encrypt_AES_128_ECB(plaintext, key)

    return ciphertext

def find_blocksize(oracle, fill_len=0) -> (int, int):
    """find_blocksize: find the block size up to a maximum of 512 bits.
    Returns a tuple of int containing the block size and the number of filling
    characters entered to get a new block. If the blocksize was not found,
    returns 0.

    :param oracle:
    :type oracle: encryption_oracle_ecb

    :rtype: tuple
    """
    # max block size to check for 
    bsize_max = 64
    plaintext = b'A'*fill_len
    ciphertext = oracle(plaintext)
    ct_len = len(ciphertext)
    blocksize = 0
    for i in range(bsize_max+1):
        plaintext += b'A'
        ciphertext = oracle(plaintext)
        if len(ciphertext) != ct_len:
            blocksize = len(ciphertext)-ct_len
            break

    return blocksize, i+1
def byte_at_a_time_pre_post(oracle: encryption_oracle_ecb_pre_post, bsize: int,
        fill_len: int, pre_position: int) -> bytes:
    """byte_at_a_time_post

    :param oracle:
    :type oracle: encryption_oracle_ecb
    :param bsize:
    :type bsize: int

    :rtype: bytes
    """
    print('[+] Trying byte at a time attack:')
    # fill the last block used by the random prefix to reach a full bsize block
    fill = b'A' * fill_len
    # store here the recovered bytes
    recovered = bytearray()
    # start with a plaintext one byte short of the blocksize
    plaintext = b'A'*(bsize-1)
    # add the filling
    plaintext = fill + plaintext
    # compute how many blocks we have to crack
    ciphertext = oracle(fill+b'')
    if len(ciphertext)%bsize != 0:
        raise ValueError
    n_blocks = int(len(ciphertext)/bsize)
    # subtract the blocks used by the random prefix
    n_blocks = n_blocks - pre_position//bsize
    # get the length of the secret text, need to offset this with fill_len
    bsize, filling = find_blocksize(oracle, fill_len)
    # this is the length of the secret string
    s_length = len(ciphertext)-filling-pre_position
   
    # compute just once all the plaintexts (P_n) and corresponding
    # ciphertexts(C_n) then store them in ct_store
    ct_store = {}
    for i in range(bsize):
        plaintext = b'A'*(bsize-1-i)
        plaintext = fill + plaintext
        ciphertext = oracle(plaintext)
        ct_store[i] = {}
        ct_store[i]['C'] = ciphertext
        ct_store[i]['P'] = plaintext
    
    # decrypt one block at a time
    for b in range(n_blocks):
        # cycle through every byte in the block
        for i in range(bsize):
            # try every possible value [0, 255] in one byte
            for k in range(256):
                # craft the input using the current plaintext P_n, the bytes
                # recovered so far and the current test value
                crafted_input = ct_store[i]['P']+recovered+k.to_bytes(1, sys.byteorder)
                # get the ciphertext using the crafted input
                cipher_crafted = oracle(crafted_input)
                # check if the b-th blocks of the two ciphers match
                if (cipher_crafted[b*bsize+pre_position:b*bsize+pre_position+bsize] ==
                        ct_store[i]['C'][b*bsize+pre_position:b*bsize+pre_position+bsize]):
                    # if they match, we got a new byte. store it
                    recovered += k.to_bytes(1, sys.byteorder)
                    sys.stdout.write('\r recovered {0}/{1} bytes: {2}'.format(
                        len(recovered),
                        s_length,
                        repr(recovered.decode())
                        ))
                    # check if we recovered all the characters
                    if len(recovered) == s_length:
                        sys.stdout.write('\n')
                        sys.stdout.flush()
                        #return the secret string
                        return recovered
                    # exit the bruteforce loop
                    break
if __name__ == '__main__':
    
    # get the blocksize used by the oracle
    bsize, fill = find_blocksize(encryption_oracle_ecb_pre_post)
    filling = b'A'
    # give an increasing length input to the oracle until you fill completely
    # two blocks (ECB is used, you'll find two equal blocks in the cipher)
    while True:
        ciphertext = encryption_oracle_ecb_pre_post(filling)
        blocks = find_repeating_blocks(ciphertext)
        if 2 in blocks.values():
            for b,n in blocks.items():
                if n == 2:
                    block = b
                    break
            break
        filling += b'A'
    # this is the how much space of its last block is NOT used by the random prefix
    # because it doesn't match the blocksize
    fill_len = len(filling) - bsize*2
    # here's where the two filled blocks start (first position after the
    # "filled" blocks used by the random prefix)
    position = ciphertext.find(block+block)
    recovered = byte_at_a_time_pre_post(encryption_oracle_ecb_pre_post, bsize,
            fill_len, position)
    print(recovered)
