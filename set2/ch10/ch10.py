import base64
import sys
sys.path.append('../../libs')
from cryptopals import (encrypt_AES_128_ECB,
        decrypt_AES_128_ECB,
        pad_pkcs7,
        unpad_pkcs7,
        fixed_xor)

def encrypt_AES_128_CBC(plaintext: bytes, key: bytes, iv: bytes) -> bytes:
    """encrypt_AES_128_CBC

    :param plaintext: the plaintext, not padded
    :type plaintext: bytes
    :param key: the key
    :type key: bytes
    :param iv: the initialisation vector
    :type iv: bytes
    
    :return: the ciphertext
    :rtype: bytes
    """
    # block size is 128 bits (16 bytes)
    bsize = 16
    # pad the plaintext to the correct size
    plaintext = pad_pkcs7(plaintext, bsize)
    plen = len(plaintext)
    ciphertext = bytearray()
    # start the encryption
    # the first block is XORed with the IV
    plain_block = plaintext[:bsize]
    ct_tmp = encrypt_AES_128_ECB(fixed_xor(plain_block, iv), key)
    ciphertext += ct_tmp
    # loop the remaining blocks
    for i in range(1, plen//bsize):
        # this is the plaintext block about to get encrypted
        plain_block = plaintext[i*bsize:i*bsize+bsize]
        # XOR the plaintext with the previoud ciphertext
        pt_tmp = fixed_xor(plain_block, ct_tmp)
        # encrypt the block
        ct_tmp = encrypt_AES_128_ECB(pt_tmp, key)
        # append to the final ciphertext
        ciphertext += ct_tmp
    return bytes(ciphertext)

def decrypt_AES_128_CBC(ciphertext: bytes, key: bytes, iv: bytes) -> bytes:
    """decrypt_AES_128_CBC

    :param ciphertext: the ciphertext to decrypt
    :type ciphertext: bytes
    :param key: the key
    :type key: bytes
    :param iv: initialisation vector
    :type iv: bytes
    
    :return: the plaintext unpadded
    :rtype: bytes
    """
    # block size is 128 bits (16 bytes)
    bsize = 16
    ctlen = len(ciphertext)
    plaintext = bytes()
    # start the decryption
    # the first block is XORed with the IV
    ciph_block = ciphertext[:bsize]
    # decrypt the block
    pt_tmp = decrypt_AES_128_ECB(ciph_block, key)
    # XOR against IV and append to final plaintext
    plaintext += fixed_xor(pt_tmp, iv)
    # loop the remaining blocks
    for i in range(1, ctlen//bsize):
        # the ciphertext block about to get decrypted
        ciph_block = ciphertext[i*bsize:i*bsize+bsize]
        # decrypt the block
        pt_tmp = decrypt_AES_128_ECB(ciph_block, key)
        # XOR the plaintext against the previous ciphertext block and append to
        # the final plaintext
        plaintext += fixed_xor(pt_tmp,
                ciphertext[(i-1)*bsize:(i-1)*bsize+bsize])
    # unpad the plaintext
    plaintext = unpad_pkcs7(plaintext, bsize)
    return plaintext

if __name__ == '__main__':
    with open('10.txt', 'r') as f:
        ciphertext = f.read()
    ciphertext = base64.b64decode(ciphertext)
    key = b'YELLOW SUBMARINE'
    iv = bytes(16)
    plain = decrypt_AES_128_CBC(ciphertext, key, iv)
    print(plain.decode())
