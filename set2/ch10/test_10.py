import unittest
import ch10

class test_ch10(unittest.TestCase):

    def setUp(self):
        pass
    def test_example(self):
        pass
    def test_aes_ecb(self):
        test_pt = b'this is a test m8'
        test_pt = ch10.pad_pkcs7(test_pt, 16)
        key = bytes(b'k'*16)
        ct = ch10.encrypt_AES_128_ECB(test_pt, key)
        pt = ch10.decrypt_AES_128_ECB(ct, key)
        pt = ch10.unpad_pkcs7(pt, 16)
        pt = ch10.pad_pkcs7(pt, 16)
        test_ct = ch10.encrypt_AES_128_ECB(pt, key)
        self.assertEqual(test_pt, pt)
        self.assertEqual(test_ct, ct)
    
    def test_aes_cbc_long(self):
        test_plain = b'this is the plaintext for test '*100
        key = b'k'*16
        iv = bytearray(16)
        ct = ch10.encrypt_AES_128_CBC(test_plain, key, iv)
        pt = ch10.decrypt_AES_128_CBC(ct, key, iv)
        self.assertEqual(test_plain, pt)
        test_ct = ch10.encrypt_AES_128_CBC(pt, key,  iv)
        self.assertEqual(test_ct, ct)

    def test_aes_cbc_short(self):
        test_plain = b'less than 16'
        key = b'k'*16
        iv = bytearray(16)
        ct = ch10.encrypt_AES_128_CBC(test_plain, key, iv)
        pt = ch10.decrypt_AES_128_CBC(ct, key, iv)
        self.assertEqual(test_plain, pt)
        test_ct = ch10.encrypt_AES_128_CBC(pt, key,  iv)
        self.assertEqual(test_ct, ct)

if __name__ == '__main__':
    unittest.main()

