import ch15
import re
import unittest

class test_ch15(unittest.TestCase):

    def setUp(self):
        pass
    
    def test_valid(self):
        padded = b"ICE ICE BABY\x04\x04\x04\x04"
        unpadded = b"ICE ICE BABY"
        test = ch15.unpad_pkcs7(padded)
        self.assertEqual(test, unpadded)
    
    def test_wrong_num(self):
        padded = b"ICE ICE BABY\x05\x05\x05\x05"
        pattern = re.compile('.*')
        self.assertRaisesRegex(ValueError, pattern, ch15.unpad_pkcs7, padded)

    def test_wrong_val(self):
        padded = b"ICE ICE BABY\x01\x02\x03\x04"
        pattern = re.compile('.*')
        self.assertRaisesRegex(ValueError, pattern, ch15.unpad_pkcs7, padded)

if __name__ == '__main__':
    unittest.main()
