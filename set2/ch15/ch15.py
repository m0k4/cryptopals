import sys

def unpad_pkcs7(message: bytes) -> bytes:
    """unpad_pkcs7: unpad the message using PKCS#7.
    Return the unpadded message as bytes.
    Raise ValueError if the padding is not valid.

    :param message:
    :type message: bytes

    :rtype: bytes
    """
    # get the last padding value
    pad_val = int.from_bytes(message[-1:], sys.byteorder)
    # count how many bytes with pad_val value are contained in the last pad_val
    # bytes of the message
    pad_num = message.count(message[-1:], -pad_val)
    # check if the number of pad_val bytes and pad_val are the same
    if pad_num != pad_val:
        raise ValueError
    return message[:-pad_val]

if __name__ == '__main__':
    pass
