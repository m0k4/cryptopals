import re
import sys
sys.path.append('../../libs')
from cryptopals import (encrypt_AES_128_ECB,
        decrypt_AES_128_ECB,
        pad_pkcs7,
        unpad_pkcs7)

def parse_key_val(cipher_encoded: bytes)-> dict:
    """parse_key_val: parsing routine to extract the key-value pairs from an
    encoded string and return them as a dictionary.

    :param cipher_encoded:
    :type cipher_encoded: bytes

    :rtype: dict
    """
    plaintext = decrypt_profile(cipher_encoded, b'yellow submarine')
    plaintext = plaintext.decode()
    pairs = plaintext.split(sep='&')
    cookie = {}
    struct = re.compile('(\S+)=(\S+)')
    for i in pairs:
        pair = struct.search(i)
        cookie[pair.group(1)] = pair.group(2)
    return cookie

def profile_for(email: str) -> bytes:
    """profile_for: build the user profile given the email of the user.
    Forbidden characters in the email string such as '&' and '=' are eaten.
    Returns the encoded and encrypted profile as bytes.

    :param email:
    :type email: str

    :rtype: bytes
    """
    #remove &
    email = email.replace('&', '')
    #remove =
    email = email.replace('=', '')

    profile = [('email',email),
            ('uid', 10),
            ('role', 'user')]
    encoded = ''
    for p in profile:
        k,v = p
        if encoded:
            encoded += '&{0}={1}'.format(k,v)
        else:
            encoded = '{0}={1}'.format(k,v)
    plaintext = encoded.encode()
    ciphertext = encrypt_profile(plaintext, b'yellow submarine')
    return ciphertext

def encrypt_profile(profile: bytes, key: bytes) -> bytes:
    """encrypt_profile: encrypt the user profile using the key "key".
    Returns the encrypted profile as bytes.

    :param profile: the user profile, unpadded
    :type profile: bytes
    :param key: the encryption key
    :type key: bytes

    :rtype: bytes
    """
    profile = pad_pkcs7(profile, 16)
    ciphertext = encrypt_AES_128_ECB(profile, key)
    return ciphertext

def decrypt_profile(ciphertext: bytes, key: bytes) -> bytes:
    """decrypt_profile: decrypt the user profile using the key "key".
    Returns the unpadded plaintext of the profile as bytes.

    :param ciphertext: the encrypted profile
    :type ciphertext: bytes
    :param key: the encryption key
    :type key: bytes

    :rtype: bytes
    """
    plaintext = decrypt_AES_128_ECB(ciphertext, key)
    plaintext = unpad_pkcs7(plaintext, 16)
    return plaintext


if __name__ == '__main__':
    # get the block containing the string admin followed by the padding
    # (11 bytes)
    val = 11
    fill = val.to_bytes(1, sys.byteorder)*val
    # 'me@nsa.gov' fills the first block the rest will be in the second block
    crafted = 'me@nsa.govadmin'+fill.decode()
    # get the ciphertext
    c1 = profile_for(crafted)
    # cut the second block
    c11 = c1[16:32]
    # create the profile for jerry@nsa.gov
    c2 = profile_for('jerry@nsa.gov')
    #  cut the first two blocks
    c21 = c2[:32]
    # paste the blocks
    ciphertext = c21+c11
    # get the encoded profile with admin role
    plaintext = parse_key_val(ciphertext)
    print(plaintext)
