import unittest
import ch09

class test_ch09(unittest.TestCase):

    def setUp(self):
        pass

    def test_example(self):
        message = b'YELLOW SUBMARINE'
        bsize = 20
        padded = ch09.pad_pkcs7(message, bsize)
        self.assertEqual(padded, b'YELLOW SUBMARINE\x04\x04\x04\x04')

if __name__ == '__main__':
    unittest.main()
