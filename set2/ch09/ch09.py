import sys

def pad_pkcs7(message: bytes, bsize: int) -> bytes:
    """pad_pkcs7: pad the message to the blocksize specified by bsize using
    the PKCS#7 padding scheme.

    https://tools.ietf.org/html/rfc5652#section-6.3

    :param message: the message to pad.
    :type message: bytes
    :param bsize: the block size to use
    :type bsize: int

    :return: the padded message
    :rtype: bytes
    """
    if bsize<2 or bsize>255:
        raise ValueError

    mlen = len(message)
    pad_size = bsize - (mlen % bsize)
    pad_val = pad_size.to_bytes(1, sys.byteorder, signed=False)
    padding = pad_val * pad_size
    return message + padding
