import sys
sys.path.append('../../libs/')
from cryptopals import single_xor_guess, fixed_xor

with open('4.txt', 'r') as f:
    guessed_plaintext = []
    for encryptd in f.read().splitlines():
        for i in single_xor_guess(bytearray.fromhex(encryptd)):
            guessed_plaintext.append(i+(encryptd,))

    # Sort by score in descending order
    guessed_plaintext.sort(key=lambda x: x[1], reverse=True)

    # Plaintext
    print(guessed_plaintext[0][0].decode('utf-8'))
    # Key
    print(guessed_plaintext[0][2])
    # Ciphertext
    print(guessed_plaintext[0][3])
