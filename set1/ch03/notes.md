XOR Table

p   k   |   c
-------------
0   0   |   0
0   1   |   1
1   0   |   1
1   1   |   0

P ^ K = C

K1 = P1

K1 ^ P1 = C1

1. in the encrypted string, search for the most frequent byte
2. that byte should be one of the most used letters which we find in the
   frequency table
3. from the encryption we got that P^K=C. Thanks to the frequency we
