import unittest
import ch03

class test_ch03(unittest.TestCase):

    def setUp(self):
        pass

    def test_example(self):
        encoded_hex_str = "1b37373331363f78151b7f2b783431333d78397828372d363c78373e783a393b3736"
        decoded_str = "Cooking MC's like a pound of bacon"

        encoded_bytes = bytes.fromhex(encoded_hex_str)
        
        guessed, score, key = ch03.single_xor_guess(encoded_bytes)[0]
        self.assertEqual(guessed,
                bytes(decoded_str, 'utf-8'))

    def test_web(self):
        """Strings generated from http://xor.pw/
        """
        encoded_hex_str = "33051f4a090b044a1e020304014a050c4a3225384a03044a070b04134a1d0b1319444a2b19191f070f4a1e020b1e4a1a4a0b040e4a1b4a0b180f4a280505060f0b044a1c0b18030b08060f19444a2b0619054a0b19191f070f4a1e020b1e4a1a5b464a1a58464a4444441a044a0b180f4a280505060f0b044a1c0b18030b08060f19444a260f1e4a4241434a080f4a1e020f4a3225384a051a0f180b1e05184a421e0203194a03194a0b4a09031809060f4a1d031e024a0b4a1a061f194a19030d044a030419030e0f4a031e43444a23044a1e0203194a090b190f464a1d0f4a0b19191f070f4a1e020b1e4a1a4a0b040e4a1b4a0b180f4a080505060f0b044a1c0b061f0f19464a0304191e0f0b0e4a050c4a1d05180e19464a0b040e4a1d0f4a0b19191f070f4a4241434a03194a1a060b03044a322538464a04051e4a08031e1d03190f4a322538444a260b1e0f184a0504464a1d0f4d06064a1f190f4a031e4a0b194a08031e1d03190f4a32253844"
        decoded_str = "You can think of XOR in many ways. Assume that p and q are Boolean variables. Also assume that p1, p2, ...pn are Boolean variables. Let (+) be the XOR operator (this is a circle with a plus sign inside it). In this case, we assume that p and q are boolean values, instead of words, and we assume (+) is plain XOR, not bitwise XOR. Later on, we'll use it as bitwise XOR."

        encoded_bytes = bytes.fromhex(encoded_hex_str)
        
        guessed = ch03.single_xor_guess(encoded_bytes)[0][0]
        self.assertEqual(guessed,
                bytes(decoded_str, 'utf-8'))   
if __name__ == '__main__':
    unittest.main()
