import re
import sys
sys.path.append('../../libs/')
from cryptopals import fixed_xor

def single_xor_guess(xord_buf: bytearray) -> list:
    """Guess the plaintext P from the ciphertext C (xord_buf), obtained by
    XORing every character of P with the single character K.

    Both the key K and the plaintext P are encoded using ASCII and the
    plaintext is in english.

    :param xord_buf: the XORed ciphertext
    :type xord_buf: bytearray
    
    :return: an ordered list of tuples, each one containing the guessed plaintext as
    bytes object, the likelihood of being english text as a float number and
    key used to decrypt the ciphertext.
    :rtype: list
    """
    # Load letter frequency from file.
    freq_format = re.compile('(.)\s+(\S+)')
    freq_table = {}
    with open('eng_freq.txt', 'r') as f:
        for l in f.read().splitlines():
            score = freq_format.search(l)
            freq_table[score.group(1)] = float(score.group(2))
    
    def score_plaintext(plain: str, freq_table: dict) -> float:
        """Score the likelihood of a string being a plaintext of the language
        described by the frequency table.

        :param plain: the plaintext string
        :type plain: str
        :param freq_table: the character frequency table for a language
        :type freq_table: dict
        
        :rtype: float
        """
        score = 0
        for c in plain:
            if c in freq_table.keys():
                score += freq_table[c]
        return score

    # Get a list of guessed plaintext, in descending order from most to less
    # probable
    guessed_plaintext = []
    for i in range(128):
        key_buf = bytes(chr(i)*len(xord_buf), 'utf-8')
        plaintext = fixed_xor(xord_buf, key_buf)
        try:
            score = score_plaintext(plaintext.decode('utf-8'), freq_table)
        except:
            score = 0
        guessed_plaintext.append((plaintext, score, chr(i)))
    
    # Sort by score in descending order
    guessed_plaintext.sort(key=lambda x: x[1], reverse=True)

    return guessed_plaintext

if __name__ == '__main__':
    xord_str = input('Enter the encoded string:')
    xord_buf = bytearray.fromhex(xord_str)
    print(single_xor_guess(xord_buf))
