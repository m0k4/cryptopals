import unittest
import ch02

class test_ch02(unittest.TestCase):

    def setUp(self):
        pass

    def test(self):
        str1 = "1c0111001f010100061a024b53535009181c"
        str2 = "686974207468652062756c6c277320657965"
        str_out = "746865206b696420646f6e277420706c6179"
        
        buf1 = bytes.fromhex(str1)
        buf2 = bytes.fromhex(str2)
        buf_out = bytes.fromhex(str_out)
        
        self.assertEqual(ch02.fixed_xor(buf1, buf2), buf_out)


if __name__ == '__main__':
    unittest.main()
