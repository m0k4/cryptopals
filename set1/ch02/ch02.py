def fixed_xor(buf1: bytes, buf2: bytes) -> bytes:
    """Takes two equal length buffers and produces their XOR combination.

    :param buf1: first buffer
    :type buf1: bytes
    :param buf2: second buffer
    :type buf2: bytes
    

    :rtype: bytes
    """
    if len(buf1) != len(buf2):
        raise ValueError
    
    xored = bytearray()
    for x,y in zip(buf1, buf2):
        xored.append(x^y)

    return bytes(xored)

if __name__ == '__main__':
    
    str1 = input("Enter string 1: ")
    str2 = input("Enter string 2: ")
    
    buf1 = bytes(str1, 'utf-8')
    buf2 = bytes(str2, 'utf-8')

    print(fixed_xor(buf1, buf2))
