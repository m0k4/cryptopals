def rep_xor(plaintext: bytearray, key: bytearray) -> bytearray:
    """Encrypt plaintext using repeating-key XOR.

    :param plaintext: the plaintext
    :type plaintext: bytearray
    :param key: the key
    :type key: bytearray

    :rtype: bytearray
    """
    ciphertext = bytearray(plaintext)
    for i in range(len(ciphertext)):
        ciphertext[i] = ciphertext[i]^key[i%len(key)]
    return ciphertext

if __name__ == '__main__':
    plaintext = input("Enter plaintext: ")
    key = input("Enter key: ")
    ciphertext = rep_xor(bytearray(plaintext.encode()), bytearray(key.encode()))
    print(ciphertext.hex())
