import unittest
import ch05

class test_ch05(unittest.TestCase):
    
    def setUp(self):
        pass
    
    def test_example(self):
        plaintext = ("Burning 'em, if you ain't quick and nimble"
                "\nI go crazy when I hear a cymbal").encode()

        key = "ICE".encode()

        ciphertext = ch05.rep_xor(bytearray(plaintext), bytearray(key))
        result =("0b3637272a2b2e63622c2e69692a23693a2a3c6324202d623d6"
                "3343c2a26226324272765272a282b2f20430a652e2c652a31243"
                "33a653e2b2027630c692b20283165286326302e27282f")
        self.assertEqual(ciphertext, bytearray.fromhex(result))

if __name__ == '__main__':
    unittest.main()
