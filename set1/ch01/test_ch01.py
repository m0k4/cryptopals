import unittest
import ch01

class test_ch01(unittest.TestCase):

    def setUp(self):
        pass

    def test_example(self):
        hex_str = "49276d206b696c6c696e6720796f757220627261696e206c696b65206120706f69736f6e6f7573206d757368726f6f6d"
        b64_str = "SSdtIGtpbGxpbmcgeW91ciBicmFpbiBsaWtlIGEgcG9pc29ub3VzIG11c2hyb29t"

        hex_bytes = bytes.fromhex(hex_str)
        
        self.assertEqual(ch01.hex2b64(hex_bytes), b64_str.encode('utf-8'))


if __name__ == '__main__':
    unittest.main()
