import base64

def hex2b64(hex_val: bytes) -> bytes:
    """Encode a bytes object using base64 encoding.

    :param hex_val: this is the bytes object to be converted
    :type hex_val: bytes

    :rtype: bytes
    """

    return base64.b64encode(hex_val)
    


if __name__ == '__main__':
    # Store the input hex value in a string
    hex_str = input("Enter hex value: ")
    
    # Decode a hex string into a bytes object
    hex_bytes = bytes.fromhex(hex_str)
    
    print(hex2b64(hex_bytes).decode('utf-8'))
