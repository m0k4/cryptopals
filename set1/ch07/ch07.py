import base64

from cryptography.hazmat.primitives.ciphers import Cipher
from cryptography.hazmat.primitives.ciphers.algorithms import AES
from cryptography.hazmat.primitives.ciphers.modes import ECB
from cryptography.hazmat.backends import default_backend


def decode_AES_128_ECB(ciphertext: bytes, key: bytes) -> bytes:
    """decode_AES_128_ECB

    :param ciphertext:
    :type ciphertext: bytes
    :param key:
    :type key: bytes

    :rtype: bytes
    """
    backend = default_backend()
    cipher = Cipher(AES(key), ECB(), backend=backend)

    decryptor = cipher.decryptor()
    plaintext = decryptor.update(ciphertext) + decryptor.finalize()
    return plaintext

if __name__ == '__main__':
    key = b'YELLOW SUBMARINE'

    with open('7.txt','r') as f:
        b64enc = f.read()
    ciphertext = base64.b64decode(b64enc)
    plaintext = decode_AES_128_ECB(ciphertext, key)
    print(plaintext.decode())
