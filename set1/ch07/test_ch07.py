import unittest
import base64
import subprocess
import ch07

class test_ch07(unittest.TestCase):

    def setUp(self):
        pass

    def test_example(self):
        # Get the plaintext using OpenSSL
        openssl_cmd = ('openssl aes-128-ecb -K $(echo -n "YELLOW SUBMARINE" |'
        'xxd -p | head -c -1) -nosalt -d -in 7.txt -a -nopad')
        command = subprocess.run(openssl_cmd, stdout=subprocess.PIPE,
                shell=True)
        # get the plaintext using the function

        key = b'YELLOW SUBMARINE'

        with open('7.txt','r') as f:
            b64enc = f.read()
        ciphertext = base64.b64decode(b64enc)
        self.assertEqual(command.stdout,
                ch07.decode_AES_128_ECB(ciphertext, key))


if __name__ == '__main__':
    unittest.main()
