import base64
import sys
sys.path.append('../../libs/')
from cryptopals import single_xor_guess, fixed_xor
import pprint

def hamming_distance(buf01: bytearray, buf02: bytearray) -> int:
    """hamming_distance: compute the hamming distance between two strings of
    bytes returning the number of different bits.

    :param buf01:
    :type buf01: bytearray
    :param buf02:
    :type buf02: bytearray

    :rtype: int
    """
    # Check if the buffers are the same length
    if len(buf01) != len(buf02):
        raise ValueError
    # XOR the two buffers, the result will have the bits set in the position
    # where the two buffers are different.
    xored = bytearray([buf01[i]^buf02[i] for i in range(len(buf01))])
    xored = int.from_bytes(xored, 'big', signed=False)
    
    # Count the set bits.
    count = 0
    while xored:
        xored &= xored-1
        count += 1

    return count

def find_keylenght(ciphertext: bytes) -> list:
    """find_keylenght: determine the most likely KEYLENGHT based on the hamming
    distance of consecutive blocks.

    :param ciphertext:
    :type ciphertext: bytes
    
    :return: a sorted (descending by hamming distance) list of tuples
    containing the KEYLENGHT value and the the medium hamming distance
    (normalised).
    :rtype: list
    """
    key_size = range(2,60)
    score = []
    for size in key_size:
        hdist = []
        for i in range(0,len(rxor_enc)-size-1,size):
            try:
                dist = hamming_distance(rxor_enc[i:i+size],rxor_enc[i+size:i+2*size])/size
                hdist.append(dist)
            except ValueError as e:
                print('String length mismatch')
        score.append((size, sum(hdist)/int(len(rxor_enc)/size)))
    score.sort(key=lambda x: x[1])
    return score

if __name__ == '__main__':
    
    # Open and decode the ciphertext
    with open('6.txt', 'r') as f:
        b64encoded = f.read()
        rxor_enc = base64.b64decode(b64encoded)
    
    blocks = {}
    
    # Get the most likely KEYLENGHTs
    score = find_keylenght(rxor_enc)
    # Try the first three more likely keysizes
    for keysize in [score[i][0] for i in range(3)]:
        blocks[keysize] = {}
        # For each keysize get all the ciphertext value that are encrypted with
        # the same value and store them in different sets
        for i in range(len(rxor_enc)):
            # initialise the list if it doesn't exist
            blocks[keysize][i%keysize] = blocks[keysize].get(i%keysize, list())
            # store the ciphertext value
            blocks[keysize][i%keysize].append(rxor_enc[i])
    
    guessed_keys = []
    # Solve for each keylenght
    for k,v in blocks.items():
        print('[+] KeyLength {0} - guessed key: '.format(k))
        key = bytearray()
        # Treat each sets as a single xor problem
        for e,a in v.items():
            key.append(ord(single_xor_guess(bytearray(a))[0][2]))
        print(key.decode())
        guessed_keys.append(key)
    
    # Print the plaintext
    print("\n[+] Using the most likely key to decrypt:\n")
    key = guessed_keys[0]*(len(rxor_enc))
    plaintext = fixed_xor(rxor_enc, key[:len(rxor_enc)])
    print(plaintext.decode())
