import unittest
import ch06

class test_ch06(unittest.TestCase):
    
    def setUp(self):
        pass
    
    def test_hamming_distance(self):
        string01 = bytearray(b"this is a test")
        string02 = bytearray(b"wokka wokka!!!")
        result = 37
        self.assertEqual(ch06.hamming_distance(string01, string02), result)

if __name__ == '__main__':
    unittest.main()
