import sys
sys.path.append('../../libs/')
from cryptopals import find_keylenght


def find_repeating_blocks(buff: bytes ,bsize: int=16) -> dict:
    """find_repeating_blocks: search in buffer 'buff' for repeating blocks of
    size 'bsize'.

    Returns a dictionary whose keys are all the different blocks found in the
    buffer and the value of each key is the counter (an integer) of how many
    occurrence of that block was found.

    :param buff: the buffer where to search for repeating blocks
    :type buff: bytes
    :param bsize: the size of the blocks
    :type bsize: int
    
    :return: a dictionary whose keys are all the different blocks found in the
    buffer and the value of each key is the counter (an integer) of how many
    occurrence of that block was found.
    :rtype: list
    """
    blen = len(buff)
    bnum = blen//bsize
    blocks = {}
    for i in range(0, bnum*bsize, bsize):
        blocks[buff[i:i+bsize]] = blocks.get(buff[i:i+bsize], 0) + 1
    return blocks

if __name__ == '__main__':
    with open('8.txt', 'r') as f:
        ciphers = f.read().splitlines()
    candidate = {}
    # Loop through all ciphertexts
    for c in ciphers:
        # Compute repeating blocks in the ciphertext
        stat = find_repeating_blocks(bytes(c, 'utf-8'))
        for k,v in stat.items():
            if v > 1:
                block = candidate.get(c, dict())
                block[k] = v
                candidate[c] = block
    print(candidate)
