import base64
from cryptography.hazmat.primitives.ciphers import Cipher
from cryptography.hazmat.primitives.ciphers.algorithms import AES
from cryptography.hazmat.primitives.ciphers.modes import ECB
from cryptography.hazmat.backends import default_backend
from random import SystemRandom
import re
import sys

def hex2b64(hex_val: bytes) -> bytes:
    """Encode a bytes object using base64 encoding.

    :param hex_val: this is the bytes object to be converted
    :type hex_val: bytes

    :rtype: bytes
    """

    return base64.b64encode(hex_val)
    
def fixed_xor(buf1: bytes, buf2: bytes) -> bytes:
    """Takes two equal length buffers and produces their XOR combination.

    :param buf1: first buffer
    :type buf1: bytes
    :param buf2: second buffer
    :type buf2: bytes
    
    :rtype: bytes
    """
    if len(buf1) != len(buf2):
        print("Buf1 has length {0}\nBuf2 has length {1}".format(len(buf1),
             len(buf2)))
        raise ValueError
    
    xored = bytearray()
    for x,y in zip(buf1, buf2):
        xored.append(x^y)

    return bytes(xored)

def xor_buf(buf1: bytes, buf2: bytes) -> bytes:
    """Takes two equal length buffers and produces their XOR combination.

    :param buf1: first buffer
    :type buf1: bytes
    :param buf2: second buffer
    :type buf2: bytes
    
    :rtype: bytes
    """
    xored = bytearray()
    for x,y in zip(buf1, buf2):
        xored.append(x^y)

    return bytes(xored)

def score_plaintext(plain: str, freq_table: dict) -> float:
    """Score the likelihood of a string being a plaintext of the language
    described by the frequency table.

    :param plain: the plaintext string
    :type plain: str
    :param freq_table: the character frequency table for a language
    :type freq_table: dict
    
    :rtype: float
    """
    score = 0
    for c in plain:
        if c in freq_table.keys():
            score += freq_table[c]
    return score

def single_xor_guess(xord_buf: bytearray) -> list:
    """Guess the plaintext P from the ciphertext C (xord_buf), obtained by
    XORing every character of P with the single character K.

    Both the key K and the plaintext P are encoded using ASCII and the
    plaintext is in english.

    :param xord_buf: the XORed ciphertext
    :type xord_buf: bytearray
    
    :return: an ordered list of tuples, each one containing the guessed plaintext as
    bytes object, the likelihood of being english text as a float number and
    the key used to decrypt it
    :rtype: list
    """
    # Load letter frequency from file.
    freq_format = re.compile('(.)\s+(\S+)')
    freq_table = {}
    with open('eng_freq.txt', 'r') as f:
        for l in f.read().splitlines():
            score = freq_format.search(l)
            freq_table[score.group(1)] = float(score.group(2))
    
    # Get a list of guessed plaintext, in descending order from most to less
    # probable
    guessed_plaintext = []
    for i in range(128):
        key_buf = bytes(chr(i)*len(xord_buf), 'utf-8')
        plaintext = fixed_xor(xord_buf, key_buf)
        try:
            score = score_plaintext(plaintext.decode('utf-8'), freq_table)
        except:
            score = 0
        guessed_plaintext.append((plaintext, score, chr(i)))
    
    # Sort by score in descending order
    guessed_plaintext.sort(key=lambda x: x[1], reverse=True)

    return guessed_plaintext

def rep_xor(plaintext: bytearray, key: bytearray) -> bytearray:
    """Encrypt plaintext using repeating-key XOR.

    :param plaintext:
    :type plaintext: bytearray
    :param key:
    :type key: bytearray

    :rtype: bytearray
    """
    for i in range(len(plaintext)):
        plaintext[i] = plaintext[i]^key[i%len(key)]
    return plaintext

def hamming_distance(buf01: bytearray, buf02: bytearray) -> int:
    """hamming_distance: compute the hamming distance between two strings of
    bytes returning the number of different bits.

    :param buf01:
    :type buf01: bytearray
    :param buf02:
    :type buf02: bytearray

    :rtype: int
    """
    # Check if the buffers are the same length
    assert len(buf01) == len(buf02)
    # XOR the two buffers, the result will have the bits set in the position
    # where the two buffers are different.
    xored = bytearray([buf01[i]^buf02[i] for i in range(len(buf01))])
    xored = int.from_bytes(xored, 'big', signed=False)
    
    # Count the set bits.
    count = 0
    while xored:
        xored &= xored-1
        count += 1

    return count

def find_repeating_blocks(buff: bytes ,bsize: int=16) -> dict:
    """find_repeating_blocks: search in buffer 'buff' for repeating blocks of
    size 'bsize'.

    Returns a dictionary whose keys are all the different blocks found in the
    buffer and the value of each key is the counter (an integer) of how many
    occurrence of that block was found.

    :param buff: the buffer where to search for repeating blocks
    :type buff: bytes
    :param bsize: the size of the blocks
    :type bsize: int
    
    :return: a dictionary whose keys are all the different blocks found in the
    buffer and the value of each key is the counter (an integer) of how many
    occurrence of that block was found.
    :rtype: list
    """
    blen = len(buff)
    bnum = blen//bsize
    blocks = {}
    for i in range(0, bnum*bsize, bsize):
        blocks[buff[i:i+bsize]] = blocks.get(buff[i:i+bsize], 0) + 1
    return blocks

def find_keylenght(ciphertext: bytes) -> list:
    """find_keylenght: determine the most likely KEYLENGHT based on the hamming
    distance of consecutive blocks.

    :param ciphertext:
    :type ciphertext: bytes
    
    :return: a sorted (descending by hamming distance) list of tuples
    containing the KEYLENGHT value and the the medium hamming distance
    (normalised).
    :rtype: list
    """
    key_size = range(2,10)
    score = []
    for size in key_size:
        hdist = []
        for i in range(0,len(ciphertext)-size-1,size):
            try:
                dist = hamming_distance(ciphertext[i:i+size],ciphertext[i+size:i+2*size])/size
                hdist.append(dist)
            except AssertionError as e:
                pass
                #print('String length mismatch')
        score.append((size, sum(hdist)/int(len(ciphertext)/size)))
    score.sort(key=lambda x: x[1])
    return score

def decrypt_AES_128_ECB(ciphertext: bytes, key: bytes) -> bytes:
    """decode_AES_128_ECB

    :param cipher:
    :type cipher: bytes
    :param key:
    :type key: bytes

    :rtype: bytes
    """
    backend = default_backend()
    cipher = Cipher(AES(key), ECB(), backend=backend)

    decryptor = cipher.decryptor()
    plaintext = decryptor.update(ciphertext) + decryptor.finalize()
    return plaintext

def encrypt_AES_128_ECB(plaintext: bytes, key: bytes) -> bytes:
    """encrypt_AES_128_ECB

    :param plaintext:
    :type plaintext: bytes
    :param key:
    :type key: bytes

    :rtype: bytes
    """
    backend = default_backend()
    cipher = Cipher(AES(key), ECB(), backend=backend)
    
    encryptor = cipher.encryptor()
    ciphertext = encryptor.update(plaintext) + encryptor.finalize()
    return ciphertext

def pad_pkcs7(message: bytes, bsize: int) -> bytes:
    """pad_pkcs7: pad the message to the blocksize specified by bsize using
    the PKCS#7 padding scheme.

    https://tools.ietf.org/html/rfc5652#section-6.3

    :param message:
    :type message: bytes
    :param bsize:
    :type bsize: int

    :return: the padded message
    :rtype: bytes
    """
    if bsize<2 or bsize>255:
        raise ValueError

    mlen = len(message)
    psize = bsize - (mlen % bsize)
    pval = psize.to_bytes(1, sys.byteorder, signed=False)
    padding = pval * psize
    return message + padding

def unpad_pkcs7(message: bytes, bsize: int) -> bytes:
    if bsize<2 or bsize>255:
        raise ValueError
    pad_val = int.from_bytes(message[-1:], sys.byteorder)

    return message[:-pad_val]

def encrypt_AES_128_CBC(plaintext: bytes, key: bytes, iv: bytes) -> bytes:
    """encrypt_AES_128_CBC

    :param plaintext: the plaintext, not padded
    :type plaintext: bytes
    :param key: the key
    :type key: bytes
    :param iv: the initialisation vector
    :type iv: bytes
    
    :return: the ciphertext
    :rtype: bytes
    """
    # block size is 128 bits (16 bytes)
    bsize = 16
    # pad the plaintext to the correct size
    plaintext = pad_pkcs7(plaintext, bsize)
    plen = len(plaintext)
    ciphertext = bytearray()
    # start the encryption
    # the first block is XORed with the IV
    plain_block = plaintext[:bsize]
    ct_tmp = encrypt_AES_128_ECB(fixed_xor(plain_block, iv), key)
    ciphertext += ct_tmp
    # loop the remaining blocks
    for i in range(1, plen//bsize):
        # this is the plaintext block about to get encrypted
        plain_block = plaintext[i*bsize:i*bsize+bsize]
        # XOR the plaintext with the previoud ciphertext
        pt_tmp = fixed_xor(plain_block, ct_tmp)
        # encrypt the block
        ct_tmp = encrypt_AES_128_ECB(pt_tmp, key)
        # append to the final ciphertext
        ciphertext += ct_tmp
    return bytes(ciphertext)

def decrypt_AES_128_CBC(ciphertext: bytes, key: bytes, iv: bytes) -> bytes:
    """decrypt_AES_128_CBC

    :param ciphertext: the ciphertext to decrypt
    :type ciphertext: bytes
    :param key: the key
    :type key: bytes
    :param iv: initialisation vector
    :type iv: bytes
    
    :return: the plaintext unpadded
    :rtype: bytes
    """
    # block size is 128 bits (16 bytes)
    bsize = 16
    ctlen = len(ciphertext)
    plaintext = bytes()
    # start the decription
    # the first block is XORed with the IV
    ciph_block = ciphertext[:bsize]
    # decrypt the block
    pt_tmp = decrypt_AES_128_ECB(ciph_block, key)
    # XOR against IV and append to final plaintext
    plaintext += fixed_xor(pt_tmp, iv)
    # loop the remaining blocks
    for i in range(1, ctlen//bsize):
        # the ciphertext block about to get decrypted
        ciph_block = ciphertext[i*bsize:i*bsize+bsize]
        # decrypt the block
        pt_tmp = decrypt_AES_128_ECB(ciph_block, key)
        # XOR the plaintext against the previous ciphertext block and append to
        # the final plaintext
        plaintext += fixed_xor(pt_tmp,
                ciphertext[(i-1)*bsize:(i-1)*bsize+bsize])
    # unpad the plaintext
    plaintext = unpad_pkcs7(plaintext, bsize)
    return plaintext

def generate_random_bytes(bsize: int) -> bytes:
    """generate_random_bytes: return a bytes-like object of size 'bsize'
    containing random content.

    :param bsize: the size of the buffer
    :type bsize: int
    
    :return: a random buffer of size 'bsize'
    :rtype: bytes
    """
    rand = bytearray(bsize)
    rng = SystemRandom()
    for i in range(len(rand)):
        rand[i] = rng.randrange(256)
    return bytes(rand)

def encryption_oracle_mode(plaintext: bytes) -> (bytes, str):
    """encryption_oracle: an encryption oracle that:
    + append before the given plaintext a random value buffer of random size
    between 5 and 10 bytes.
    + append after the given plaintext a random value buffer of random size
    between 5 and 10 bytes.
    + use randomly (0.5 probability each) AES 128 CBC or AES 128 ECB
    + generate a random key
    + generate a random IV
    
    Returns a tuple containing the ciphertext and a string that state the block
    mode used:
    + 0 for ECB
    + 1 for CBC

    :param bytes: the plaintext
    :param str:

    :rtype: (bytes,str)
    """
    rng = SystemRandom()
    pre_size = rng.randrange(5,11)
    pre = generate_random_bytes(pre_size)
    post_size = rng.randrange(5,11)
    post = generate_random_bytes(post_size)
    plaintext = pre + plaintext + post
    key = generate_random_bytes(16)
    mode = rng.randrange(0,2)
    if mode == 0:
        # use ECB
        plaintext = pad_pkcs7(plaintext, 16)
        ciphertext = encrypt_AES_128_ECB(plaintext, key)
    elif mode == 1:
        #use CBC
        iv = generate_random_bytes(16)
        ciphertext = encrypt_AES_128_CBC(plaintext, key, iv)
    else:
        raise Exception

def find_blocksize(oracle) -> (int, int):
    """find_blocksize: find the block size up to a maximum of 512 bits.
    Returns a tuple of int containing the block size and the number of filling
    characters entered to get a new block. If the blocksize was not found,
    returns 0.

    :param oracle:
    :type oracle: encryption_oracle_ecb

    :rtype: tuple
    """
    # max block size to check for 
    bsize_max = 64
    plaintext = b''
    ciphertext = oracle(plaintext)
    ct_len = len(ciphertext)
    blocksize = 0
    for i in range(bsize_max+1):
        plaintext += b'A'
        ciphertext = oracle(plaintext)
        if len(ciphertext) != ct_len:
            blocksize = len(ciphertext)-ct_len
            break

    return blocksize, i+1
